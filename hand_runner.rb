# frozen_string_literal: true

require_relative 'card'
require_relative 'hand'

@card_one = Card.new('Diamonds', 'Jack')
@card_two = Card.new('Clubs', 'Ace')

@hand = Hand.new
@hand.add_card @card_one
@hand.add_card @card_two

puts @hand

# frozen_string_literal: true

require './card.rb'
require './hand.rb'

RSpec.describe Hand do
  before do
    @hand = Hand.new
  end

  it 'responds to dealt_cards' do
    expect(@hand).to respond_to(:dealt_cards)
  end

  describe '#add_card' do
    it 'responds to add_card' do
      expect(@hand).to respond_to(:add_card)
    end

    it 'returns the correct properties of cards added' do
      @card_one = Card.new('Diamonds', 'Jack')
      @card_two = Card.new('Clubs', 'Ace')

      @hand.add_card @card_one
      @hand.add_card @card_two

      expect(@hand.dealt_cards.size).to eq(2)
      expect(@hand.dealt_cards.first.suit).to eq('Diamonds')
      expect(@hand.dealt_cards.first.rank).to eq('Jack')
      expect(@hand.dealt_cards.last.suit).to eq('Clubs')
      expect(@hand.dealt_cards.last.rank).to eq('Ace')
    end
  end

  describe '#get_value' do
    it 'responds to get_value' do
      expect(@hand).to respond_to(:get_value)
    end

    it 'return correct value with no Ace' do
      @card_one = Card.new('Diamonds', 'Jack')
      @card_two = Card.new('Clubs', '9')

      @hand.add_card @card_one
      @hand.add_card @card_two

      expect(@hand.get_value).to eq(19)
    end

    it 'return correct value with an Ace and a Jack' do
      @card_one = Card.new('Diamonds', 'Jack')
      @card_two = Card.new('Clubs', 'Ace')

      @hand.add_card @card_one
      @hand.add_card @card_two

      expect(@hand.get_value).to eq(21)
    end
  end

  describe 'Hand output' do
    it "returns the correct output if 'show' is true for all cards" do
      @hand.add_card Card.new('Diamonds', 'Jack')
      @hand.add_card Card.new('Clubs', 'Ace')

      expect(@hand.to_s).to eq('Jack of Diamonds, Ace of Clubs, Total value: 21')
    end

    it "returns the correct output if 'show' is false for one cards" do
      @card_one = Card.new('Diamonds', 'Jack')
      @card_two = Card.new('Clubs', 'Ace')
      @card_one.show = false

      @hand.add_card @card_one
      @hand.add_card @card_two

      expect(@hand.to_s).to eq('Ace of Clubs, Total value: 11')
    end
  end
end

# frozen_string_literal: true

require_relative 'card'

@card = Card.new('Clubs', '9')
puts "Suit of card: #{@card.suit}"
puts "Rank of card: #{@card.rank}"

@card.show = true
puts "Card #{@card}"
